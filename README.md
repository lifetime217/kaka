common  公共的目录
  style 公用的样式
  iconfont  提提图标   https://blog.csdn.net/nongweiyilady/article/details/74244362 
  easyModal  弹框

util  辅助目录
  wxParse 转换html的js  https://www.cnblogs.com/llkbk/p/7910454.html  借鉴
  
course  首页目录  index   首页页面
  courseDetail   课程详情
  punchClock   打卡
message   消息
  index   消息页面
owner  个人中心
  index   个人中心页面  
  enterpriseStu  企业学生（企业端）
  enterpriseTea 企业老师 （企业端）
  stuList   学生列表 （老师端）
  courseList  课程列表 
init
  search  搜索页面
  enterprise   企业列表
cardCase
  addEnterpriseCard 新增企业名片
  addCourseCard  新增课程
  enterprise 企业详情
test  工具类的时间 util.js


集合  
enterprise 公司
course 课程

message  消息
  other_openid  其他人的openid
  own_openid  邀请的openid 
  type 区分 0 私人消息  1 系统消息  
  isdelete  是否删除
  addtime  添加的时间戳
  content 内容


数据库
course 课程表
course_stu_relation  课程和学生的关联表
course_tea_relation  课程和老师的关联表
enterprise 企业表
enterprise_tea_relation  企业和老师之间的关系
message  消息表
user   用户表 



云函数  
addUser  添加用户
deleteMessage  删除单个消息
login  获取openid和appid
paginator  分页查询
queryCourse  根据课程id 查询课程详情
queryCourseToStu 查询课程对应的学生
queryEnterpirseToStu  查询企对应的学生
queryTeaToCourse 查询老师对应的课程
queryUserAndCourse 根据用户openid和课程id查询用户的基本信息和课程详情
updateUserName 修改用户名，编辑的姓名
updateUserRole  修改用户角色


  

  queryMyCourse  查询我的课程
  queryTeaToStu 查询学生列表
  deleteTeaTostu  删除学生













  
//云函数  paginator分页查询数据
调用
wx.cloud.callFunction({
      name: "paginator",
      data: {
        dbName: "counters",
        pageIndex: 1,
        pageSize: 7,
        filter:{
          "_openid":"o1U_m5e0SVFfQPy2CjSIPfi73bAg",
          "_id": "W_N9WnhEiJmg2vhp"
        }
      }
    }).then(res => {
    console.log(res);
});

//调用其它的云函数
 const res = await cloud.callFunction({
    name: "login",
    data: {
      dbName: "counters",
    }
  });
  console.log(res);

  

o1U_m5Yk89LB33fpDwIzwZKoHEOE  zhagqiang
o1U_m5RQoj_kNNqfGn-rsxC5Xj9Y  
o1U_m5dtyIVuQJ-HtPqyyUjyqtXk  hailong