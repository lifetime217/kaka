const db = wx.cloud.database();
var util = require('../../util/util.js');
/**
 * 存储打卡信息
 */
function clockStor(clockLis, course, teaList) {
  var that = this;

  that.teaCourseRequest(clockLis, course).then(function(stuList) {
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp); //打卡时间

    // console.log(teaList);
    db.collection('punch_clock_record').add({
      data: {
        punch_clock_date: util.getTimestamp(date), //打卡时间
        companyId: stuList[0].companyId,
        companyName: stuList[0].companyName,
        addtime: util.getTimestamp(date),
        courseName: stuList[0].courseName,
        courseId: stuList[0].courseId,
        isdelete: course.isdelete,
        stu_img: stuList[0].stu_img,
        stu_name: stuList[0].stu_name,
        stu_openid: stuList[0].stu_openid,

        tea_img: teaList.tea_img,
        tea_name: teaList.tea_name,
        tea_openid: teaList.tea_openid
      },
      success: res => {
        wx.showToast({
          title: '打卡成功',
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '打卡失败'
        })
        console.error('[数据库] [新增记录] 失败：', err)
      }
    })
  });


}

/**
 * 获取course_stu_relation数据
 */
function teaCourseRequest(e, course) {
  // console.log(course);
  return new Promise((resolve, reject) => {
    db.collection('course_stu_relation').where({
      stu_openid: e,
      courseId: course._id
    }).get({
      success: res => {
        resolve(res.data);
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })

  })
}



function courseShare(courseId) {
  return new Promise((resolve, reject) => {
    console.log(222);
    db.collection('course').where({
      _id: courseId,
    }).get({
      success: res => {
        resolve(res.data);
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })

  })
}

//老师同意邀请

function confirmTeacher(courseId, openid) {
  var that = this;
  var timestamp = Date.parse(new Date());
  var date = new Date(timestamp); //打卡时间
  
  // console.log(courseId);
  console.log(openid);
  // console.log(111);
  that.courseShare(courseId).then(function(course) {
    // console.log(course[0].courseName);

    //查询user用户
    db.collection('user').where({
      _openid: openid
    }).get({
      success: res => {
        var user = res.data
        if (user.length != 0) { //有

          db.collection('course_tea_relation').where({
            _openid: openid
          }).get({
            success: res_1 => {
              var teaCourse = res_1.data

              if (teaCourse.length != 0) {
                //取消授权

              } else {
                db.collection('course_tea_relation').add({
                  data: {
                    addtime: util.getTimestamp(date),
                    companyId: course[0].companyId,
                    companyName: course[0].companyName,
                    courseId: course[0]._id,
                    courseName: course[0].courseName,
                    isdelete: course[0].isdelete,
                    tea_img: '',
                    tea_name: '',
                    tea_openid: openid,
                  },
             
                  fail: err => {
                    wx.showToast({
                      icon: 'none',
                      title: '添加用户失败'
                    })
                    console.error('[数据库] [新增记录] 失败：', err)
                  }
                })

              }
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: '查询记录失败'
              })
            }
          })

        } else { //没有

          db.collection('user').add({
            data: {
              _openid: openid,
              addtime: util.getTimestamp(date),
              isEditName: false,
              new_name: '',
              old_name: '',
              user_img: '',
            },
            success: res => {
              wx.showToast({
                title: '添加用户成功',
              })
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: '添加用户失败'
              })
              console.error('[数据库] [新增记录] 失败：', err)
            }
          })

          db.collection('course_tea_relation').add({
            data: {
              addtime: util.getTimestamp(date),
              companyId: course[0].companyId,
              companyName: course[0].companyName,
              courseId: course[0]._id,
              courseName: course[0].courseName,
              isdelete: course[0].isdelete,
              tea_img: '',
              tea_name: '',
              tea_openid: openid,
            },
            success: res => {
              wx.showToast({
                title: '添加用户成功',
              })
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: '添加用户失败'
              })
              console.error('[数据库] [新增记录] 失败：', err)
            }
          })


        }
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    });
  })
}











module.exports = {
  clockStor: clockStor,
  teaCourseRequest: teaCourseRequest,
  confirmTeacher: confirmTeacher,
  courseShare: courseShare,
}