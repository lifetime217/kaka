// pages/search/search.js
var app = getApp();
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    search: "",
    searchHistory: [],
    isDelete: true,
    content: "" //弹框
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var searchHistory = app.globalData.searchHistory;
    this.setData({
      searchHistory: searchHistory
    });
    new app.ToastPannel();
  },
  // 输入框的值
  input: function(e) {
    this.setData({
      search: e.detail.value
    })
  },
  //删除
  chlickDelete: function(e) {
    console.log("删除");
  },
  // 搜索按钮
  search: function() {
    var that = this;
    const search = that.data.search;
    if (search == "") {
      that.setData({
        content: "请输入您想寻找的！"
      });
      that.show(that.data.content);
      return;
    }
    app.globalData.searchHistory.push(search);

    let pages = getCurrentPages(); //当前页面
    let prevPage = pages[pages.length - 2]; //上一页面
    var data = that.data.search;
    prevPage.setData({ //直接给上移页面赋值
      data: data,
      isOk: 'search'
    });

    wx.navigateBack({ //返回
      delta: 1
    })
  },
  /** 
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})