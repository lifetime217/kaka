// miniprogram/pages/init/enterprise/Enterprise.js
var app = getApp();
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isFirst: false, //是否是新用户
    enterpriseList: [], // 企业列表
    hasMore: true, // 判断是否还有更多的数据
    roleType: 0
  },
  // 点击卡片详情
  cardDetail: function(e) {
    const id = e.currentTarget.id;
    wx.navigateTo({
      url: '../../cardCase/enterpriseCard/enterpriseCard?id=' + id,
    })
  },
  // 开通企业
  openEnterprise: function(e) {
    wx.navigateTo({
      url: '../../cardCase/addEnterpriseCard/addEnterpriseCard'
    })
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  callPhone: function(e) {
    var phone = e.currentTarget.dataset.phone;
    this.setData({
      clickPhone: phone
    });
    this.callPhoneModal.show();
  },
  // 确定拨打电话
  confirmPhone: function() {
    var clickPhone = this.data.clickPhone;
    wx.makePhoneCall({
      phoneNumber: clickPhone
    })
    this.callPhoneModal.hide();
  },
  // 取消电话
  cancelPhone: function() {
    this.callPhoneModal.hide();
  },
  getDistance: function(la1, lo1, la2, lo2) {
    var La1 = la1 * Math.PI / 180.0;
    var La2 = la2 * Math.PI / 180.0;
    var La3 = La1 - La2;
    var Lb3 = lo1 * Math.PI / 180.0 - lo2 * Math.PI / 180.0;
    var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(La3 / 2), 2) + Math.cos(La1) * Math.cos(La2) * Math.pow(Math.sin(Lb3 / 2), 2)));
    s = s * 6378.137; //地球半径
    s = Math.round(s * 10000) / 10000;
    return s;
  },
  /**
   * 比较数组当中的大小
   */
  compare: function(property) {
    return function(a, b) {
      var value1 = a[property];
      var value2 = b[property];
      return value1 - value2;
    }
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
   * 查询企业
   */
  queryEnterprise: function() {
    var that = this;

    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "paginator",
        data: {
          dbName: "enterprise",
          pageIndex: 1,
          pageSize: 10
        }
      }).then(res => {
        resolve(res.result);

      });
    })
  },
  /**
   * 处理地址
   */
  handleAddress: function(enterPirseList, hasMore, roleType) {
    var that = this;
    if (enterPirseList.length > 0) {
      wx.getLocation({
        type: 'wgs84',
        success: function(res1) {
          // 地址获取成功
          var latitude1 = res1.latitude;
          var longitude1 = res1.longitude;
          for (var i = 0; i < enterPirseList.length; i++) {
            var latitude2 = enterPirseList[i].latitude;
            var longitude2 = enterPirseList[i].longitude;
            var distance = that.getDistance(latitude1, longitude1, latitude2, longitude2);
            if (distance * 1000 < 500) {
              enterPirseList[i].distance = "500米以内";
            } else if (distance * 1000 < 1000) {
              enterPirseList[i].distance = "1公里以内";
            } else {
              enterPirseList[i].distance = Math.round(distance) + "公里";
            }
            enterPirseList[i].distanceNum = Math.round(distance * 1000);
          }
          // 根据距离从小到大排序
          enterPirseList.sort(that.compare('distanceNum'));
          that.setData({
            enterpriseList: enterPirseList,
            roleType: roleType,
            hasMore: hasMore
          });
        },
        fail: function(res1) {
          // 地址获取失败
          that.setData({
            enterpriseList: enterPirseList,
            roleType: roleType,
            hasMore: hasMore
          });
        }
      })
    } else {
      that.setData({
        enterpriseList: [],
        roleType: roleType,
        hasMore: hasMore
      });
    }
  },
  /**
   * 收藏名片夹
   */
  keep: function(e) {
    var phone = e.currentTarget.dataset.phone;
    var name = e.currentTarget.dataset.name;
    wx.addPhoneContact({
      firstName: "A-" + name + " 打卡",
      mobilePhoneNumber: phone,
      success: function(res) {
        console.log(res);
      },
      fail: function(res) {
        console.log(res);
      },
      complete: function(res) {
        console.log(res);
      },
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var openid = app.globalData.openid;
    var roleType = app.globalData.roleType;
    // if (openid && openid != "") {
    that.queryEnterprise().then(function(result) {
      console.log(result);
      var enterPirseList = result.data;
      var hasMore = result.hasMore;
      that.handleAddress(enterPirseList, hasMore, roleType);
      that.hideTime();
    });
    // } else {
    //   app.openIdCallback = openid => {
    //     if (openid != '') {
    //       that.queryEnterprise().then(function(result) {
    //         var enterPirseList = result.data;
    //         var hasMore = result.hasMore;
    //         that.handleAddress(enterPirseList, hasMore, roleType);
    //         that.hideTime();
    //       });
    //     }
    //   }
    // }
    if (this.data.isFirst) {
      wx.hideTabBar({
        aniamtion: true
      })
    } else {

    }
    that.showLoad();



  },
  // 新增企业按钮
  addEnterprise: function() {
    wx.navigateTo({
      url: '../../cardCase/addEnterpriseCard/addEnterpriseCard'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.callPhoneModal = this.selectComponent("#callPhoneModal");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})