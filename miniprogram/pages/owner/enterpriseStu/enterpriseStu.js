// miniprogram/pages/owner/enterpriseStu/enterpriseStu.js
var app = getApp();
var roleType = app.globalData.roleType; //角色的id 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    stuList: [], //企业学生
    isNull: false, //  判断是否有数据
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  // 去重复
  removeRepeat: function(data) {
    var hash = {};
    var data1 = data.reduce(function(item, next, currentIndex, arr) {
      if (hash[next.user_id]) {
        if (next.isRelationCourse) {
          item[hash[next.user_id] - 1].courseName = item[hash[next.user_id] - 1].courseName + "," + next.courseName;
        }
      }
      hash[next.user_id] ? '' : hash[next.user_id] = true && item.push(next);
      return item
    }, [])
    return data1;
  },
  /**
   * 跳转课程
   */
  navCourse:function(e){
      
  },
  /**
   * 删除学生
   */
  deleteStu:function(){
    console.log("删除");
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.showLoad();
    var companyId = app.globalData.companyId;
    that.queryEnterpriseStu( companyId).then(function(stuList) {
      console.log(stuList);
      var newStuList = that.removeRepeat(stuList);
      that.setData({
        stuList: newStuList
      });
      that.hideTime();
    });

  },
  //查询企业旗下的学生
  queryEnterpriseStu: function(companyId) {
    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "queryEnterpriseToStu",
        data: {
          companyId: companyId
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})