// miniprogram/pages/owner/index/index.js
var app = getApp();
const db = wx.cloud.database();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    menuitems_com: [{
        text: '课程列表',
        type: 'course',
        url: '../courseList/courseList',
        icon: 'icon-kecheng',
        ismore: true
      },
      {
        text: '企业老师',
        type: 'enterPriseToTea',
        url: '../enterpriseTea/enterpriseTea',
        icon: 'icon-jiaolian1',
        ismore: true
      },
      {
        text: '企业学生',
        type: 'enterpriseToStu',
        url: '../enterpriseStu/enterpriseStu',
        icon: 'icon-xingming',
        ismore: true
      },
      {
        text: '消息列表',
        type: 'messageToEnter',
        url: '../../message/index/index',
        icon: 'icon-xiaoxi',
        ismore: true
      }
    ],
    menuitems_tea: [{
        text: '课程列表',
        type: 'course',
        url: '',
        icon: 'icon-kecheng',
        ismore: true
      },
      {
        text: '学生列表',
        type: 'stuList',
        url: '../stuList/stuList',
        icon: 'icon-ziyuan',
        ismore: true
      },
      {
        text: '消息列表',
        type: 'messageToTea',
        url: '../../message/index/index',
        icon: 'icon-xiaoxi',
        ismore: true
      }
    ],
    menuitems_par: [{
        text: '课时账单',
        type: 'punchClock',
        url: '../punchClockSituation/punchClockSituation',
        icon: 'icon-calendar',
        ismore: true
      },
      {
        text: '消息列表',
        type: 'messageToStu',
        url: '../../message/index/index',
        icon: 'icon-xiaoxi',
        ismore: true
      }
    ],
    roleType: 0, //用户角色
    lookNum: 1023, //查看次数
    collectionNum: 304, //收藏次数
    stuCount: 123, //企业学生人数
    cumulativeClassHour: 1028, //累计课时
    avgClassHour: 100, //平均课时
    courseCount: 4, //课程数量
    stuCount: 29, //学生数量
    monthClassHour: 12, //本月多少课时
    isEdit: 0, // 0代表没有默认名字的  1代表编辑之后的   2代表 有默认名字的
    userinfo: {}, //用户的信息
    userObj: {}, //查询的用户信息
  },
  // 编辑姓名完成
  finish: function(e) {
    var that = this;
    that.showLoad();
    var openid = app.globalData.openid;
    wx.cloud.callFunction({
      name: "updateUserName",
      data: {
        openid: openid,
        isEditName: true,
        new_name: that.data.editName
      }
    }).then(res => {
      if (res.result.errMsg == "collection.update:ok") {
        that.setData({
          isEdit: 2,
          new_name: that.data.editName
        });
      } else {
        that.setData({
          isEdit: 2
        });
      }
    });
    that.hideTime();

  },
  /**
   * 编辑姓名和图片
   */
  updateName: function(e) {
    var userObj = this.data.userObj;
    if (userObj.isEditName) {
      wx.navigateTo({
        url: '../updateNameAndPhoto/updateNameAndPhoto?isEditName=' + userObj.isEditName + '&name=' + userObj.new_name + '&img=' + userObj.user_img
      })
    } else {
      wx.navigateTo({
        url: '../updateNameAndPhoto/updateNameAndPhoto?isEditName=' + userObj.isEditName
      })
    }
  },

  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  // 授权获取
  confirmModal: function(e) {
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      var userinfo = e.detail.userInfo;
      console.log(userinfo);
      this.setData({
        userinfo: userinfo
      });
      var openid = app.globalData.openid;
      // 查询是否有默认的名称
      var table = db.collection("user");
      table.where({
        _openid: openid
      }).get({
        success: function(res) {
          if (res.data[0].isEditName == undefined) {
            // 没有默认名称的添加默认名称
            wx.cloud.callFunction({
              name: "updateUserName",
              data: {
                openid: openid,
                isEditName: false,
                old_name: userinfo.nickName,
                user_img: userinfo.avatarUrl
              }
            }).then(res => {
              // console.log(res);
            });
          }
        }
      })
    }
  },
  /**
   * 列表跳转页面
   */
  clickOwner: function(e) {
    const url = e.currentTarget.dataset.url;
    const ismore = e.currentTarget.dataset.ismore;
    if (ismore) {
      wx.navigateTo({
        url: url
      })
    }
  },

  /**
   * 查询个人中心的用户
   */
  queryUser: function(openid) {
    // 按照顺序执行
    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "queryUser",
        data: {
          openid: openid
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },
  /**
   * 选择的角色下标
   */
  changeRadio: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    this.swichRoleToast.hide();
    var openid = app.globalData.openid;
    that.showLoad();
    that.queryUser(openid).then(function(user) {
      that.setData({
        userObj: user.data[0],
        roleType: index
      });
      that.hideTime();
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var openid = app.globalData.openid;
    that.showLoad();
    that.queryUser(openid).then(function(user) {
      var roleType = app.globalData.roleType;
      that.setData({
        userObj: user.data[0],
        roleType: roleType
      });
      that.hideTime();
    });
  },
  // /**
  //  * 取消角色选择
  //  */
  // cancelEvent: function() {
  //   this.swichRoleToast.hide();
  // },
  // /**
  //  * 确认角色
  //  */
  // confirmEvent: function() {
  //   console.log();
  // },
  /**
   * 切换身份
   */
  changeStatus: function() {
    this.swichRoleToast.show();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.swichRoleToast = this.selectComponent("#swichRoleToast");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})