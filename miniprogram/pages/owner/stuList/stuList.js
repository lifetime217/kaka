// miniprogram/pages/owner/stuList/stuList.js
var app = getApp();
const db = wx.cloud.database();
var roleType = app.globalData.roleType;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    stuList: []
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  /**
   * 跳转课程
   */
  navCourse: function(e) {
    var courseid = e.currentTarget.dataset.courseid;
    wx.navigateTo({
      url: '../../course/courseDetail/courseDetail?id=' + courseid
    })

  },
  /**
   * 跳转公司
   */
  navCompany: function(e) {
    var companyid = e.currentTarget.dataset.companyid;
    wx.navigateTo({
      url: '../../cardCase/enterpriseCard/enterpriseCard?id=' + companyid
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var openid = app.globalData.openid;
    that.showLoad();
    if (roleType == 0) {

    } else if (roleType == 1) {
      that.queryStu(openid).then(function(stuList) {
        console.log(stuList);
        that.setData({
          stuList: stuList
        });
        that.hideTime();
      });
    }
  },
  /**
   * 删除学生
   */
  deleteStu: function(e) {
    var that = this;
    var stu_openid = e.currentTarget.dataset.useropenid;
    var stuname = e.currentTarget.dataset.stuname;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '确定要删除' + stuname + '吗？',
      success: function(res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "deleteTeaToStu",
            data: {
              id: id
            }
          }).then(res1 => {
            if (res1.result.stats.updated == 1) {
              that.showLoad();
              var openid = app.globalData.openid;
              // that.queryStu(openid).then(function(stuList) {
              //   console.log(stuList);
              //   that.setData({
              //     stuList: stuList
              //   });
              //   that.hideTime();
              // });
            }
          });
        } else if (res.cancel) {
          return false;
        }
      }
    })
  },
  /**
   * 查询学生列表
   */
  queryStu: function(openid) {
    return new Promise((resolve, reject) => {
      // 学生的列表
      // 根据老师的openid查询学生的列表
      wx.cloud.callFunction({
        name: "queryTeaToStu",
        data: {
          tea_openid: openid
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})