// miniprogram/pages/owner/enterpriseTea/enterpriseTea.js
var app = getApp();
var roleType = app.globalData.roleType; //角色的id 
Page({

  /**
   * 页面的初始数据
   */
  data: {
    teaList1: [], //企业老师
    teaList2: []

  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },

  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  // 去重复
  removeRepeat: function(data) {
    var hash = {};
    var data1 = data.reduce(function(item, next, currentIndex, arr) {

      if (hash[next.user_id]) {
        if (next.isRelationCourse) {
          item[hash[next.user_id] - 1].courseName.push({
            courseId: next.courseId,
            courseName: next.courseName.substring(0, 1)
          });
        }
      } else {
        if (next.isRelationCourse) {
          next.courseName = [{
            courseId: next.courseId,
            courseName: next.courseName.substring(0,1)
          }];
        }
        hash[next.user_id] ? '' : hash[next.user_id] = true && item.push(next);
      }
      return item
    }, [])
    return data1;
  },
  /**
   *  把数组分为两个
   */
  divideList: function(arr) {
    var teaList1 = [];
    var teaList2 = [];
    for (var i = 0; i < arr.length / 2; i++) {
      teaList1[i] = arr[i * 2];
      if (arr[i * 2 + 1] != undefined) {
        teaList2[i] = arr[i * 2 + 1];
      }
    }
    return {
      teaList1: teaList1,
      teaList2: teaList2
    }
  },
  /**
   * 跳转课程
   */
  navCourse: function(e) {
    var courseid = e.currentTarget.dataset.courseid;
    wx.navigateTo({
      url: '../../course/courseDetail/courseDetail?id=' + courseid
    })
  },
  /**
   * 跳转企业
   */
  navCompany: function(e) {
    var companyId = e.currentTarget.dataset.companyid;
    wx.navigateTo({
      url: '../../cardCase/enterpriseCard/enterpriseCard?id=' + companyId
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.showLoad();
    var companyId = app.globalData.companyId;
    that.queryEnterpriseTea(companyId).then(function(teaList) {
      var newTeaList = that.removeRepeat(teaList);
      console.log(newTeaList);
      if (newTeaList.length > 1) {
        var divideList = that.divideList(newTeaList);
        that.setData({
          teaList1: divideList.teaList1,
          teaList2: divideList.teaList2,
        });
      } else {
        that.setData({
          teaList1: newTeaList
        });
      }

      that.hideTime();
    });
  },
  /**
   * 查询企业旗下的老师
   */
  queryEnterpriseTea: function(companyId) {
    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "queryEnterpriseToTea",
        data: {
          companyId: companyId
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})