// miniprogram/pages/owner/punchClockSituation/punchClockSituation.js
var app = getApp();
var util = require('../../../util/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dateGroup: ["当月", "最近一个季度", "最近一年"], //搜索时间类型组合
    dateIndex: 0, //时间类型的下标
    punchClockList: [], //打卡记录
    date: ['日', '一', '二', '三', '四', '五', '六'],
    dateArr: [],
    year: "",
    month: "",
    today: 0,
    courseName: "",
    tea_name: "",
    punch_date: "",
    punchList: [],
    punchCount: 0, //累计打卡次数
    layout: 0, //0 代表列表查询  1代表日历
    channel: false,
    checkIndex: 0,   //选择的下标
    content:"当月",  //选择的搜索方式
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
   * 开始时间
   */
  bindTimeChange:function(e){
    console.log(e.detail.value);

  },
  /**
   * 选择下标
   */
  chiceIndex: function(e) {
    var index = e.currentTarget.dataset.index;
    console.log(index);
    this.setData({
      checkIndex:index
    });
  },
  /**
   * 选择时间的按钮
   */
  radioChange: function(e) {
    console.log(e.detail.value);
  },
  /**
   * 点击分类
   */
  choiceItem: function(e) {
    var item = e.currentTarget.dataset.item;
    if (item == 1) {
      if (this.data.channel) {
        this.setData({
          channel: false
        });
      } else {
        this.setData({
          channel: true
        });
      }
    } else {
      this.setData({
        channel: false
      });
    }
  },
  /**
   * 布局选择
   */
  layout: function(e) {
    var type = e.currentTarget.dataset.type;
    if (type == 1) {
      this.setData({
        layout: 1
      });
    } else {
      this.setData({
        layout: 0
      });
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.showLoad();
    var openid = app.globalData.openid;
    that.queryPunchClockList(openid).then(function(punchClockObj) {
      var punchClock = punchClockObj.punchClockList;
      var punchCount = punchClockObj.punchCount;
      // 日期时间
      that.setData({
        punchClockList: punchClock,
        punchCount: punchCount
      });
      that.dateInit(punchClock);
      that.hideTime();
    });
  },
  /**
   * 找到符合当前日期的数组元素的下标
   */
  findElem: function(arrayToSearch, existIndex, date) {
    var findIndexList = [];
    for (var i = 0; i < arrayToSearch.length; i++) {
      var identifier = true; //控制是否存在数组中，如没有就添加
      // 判断数组下标是否已经存在existIndex中
      for (var j = 0; j < existIndex.length; j++) {
        if (existIndex[j].indexOf(i) != -1) {
          identifier = false;
          break;
        }
      }
      if (identifier) {
        // 打卡时间戳
        var punch_clock_timestamp = arrayToSearch[i].punch_clock_date;
        // 打卡日期
        var punch_clock_date = util.formatTime(new Date(punch_clock_timestamp * 1000)).substring(0, 10);
        if (punch_clock_date == date) {
          findIndexList.push(i);
        }
      }
    }
    if (findIndexList.length > 0) {
      return findIndexList;
    } else {
      return -1;
    }
  },
  /**
   * 初始化日期
   */
  dateInit: function(punchClockList) {
    let dateArr = []; //需要遍历的日历数组数据
    let arrLen = 0; //dateArr的数组长度
    let now = new Date();
    var today = now.getDate();
    let year = now.getFullYear();
    let month = now.getMonth(); //没有+1方便后面计算当月总天数
    var lastNums; //上个月的天数
    if (month == 0) {
      lastNums = new Date(year - 1, 12, 0).getDate();
    } else {
      lastNums = new Date(year, month, 0).getDate();
    }
    var startWeek = new Date('' + year + ',' + (month + 1) + ',1').getDay(); //目标月1号对应的星期
    var dayNums = new Date(year, (month + 1), 0).getDate(); //获取目标月有多少天
    var finalWeek = new Date('' + year + ',' + (month + 1) + ',' + dayNums + '').getDay(); //最后一天的星期下标
    arrLen = startWeek + dayNums;
    var j = startWeek;
    var existIndex = []; //已存在元素的下标
    var existLength = 0; //已存在的元素的长度
    var flag = false; //控制existLength长度添加
    for (let i = 0; i < arrLen; i++) {
      let obj = {};
      if (i >= startWeek) {
        obj.day = i - startWeek + 1;
        obj.isLast = false;
        if (today >= i - startWeek + 1) {
          if (punchClockList.length != 0) {
            // 如果已经找完了就不需要再找
            if (flag) {
              for (var j = 0; j < existIndex.length; j++) {
                existLength = existLength + existIndex[j].length;
              }
            }
            if (existLength == punchClockList.length) {
              obj.isPunch = false;
            } else {
              // 找到符合日期的元素下标
              var findIndex;
              if (i - startWeek + 1 < 10) {
                findIndex = this.findElem(punchClockList, existIndex, year + "-" + (month + 1) + "-0" + (i - startWeek + 1));
              } else {
                findIndex = this.findElem(punchClockList, existIndex, year + "-" + (month + 1) + "-" + (i - startWeek + 1));
              }
              if (findIndex != -1) {
                obj.isPunch = true; //返回一个给页面判断可以点击的下标
                obj.clickPunchIndex = findIndex;
                flag = true;
                existIndex.concat(findIndex);
              } else {
                flag = false;
                obj.isPunch = false;
              }
            }
          } else {
            obj.isPunch = false;
          }
        } else {
          obj.isPunch = false;
        }
      } else {
        obj.day = lastNums - (--j);
        obj.isLast = true;
      }
      dateArr[i] = obj;
    }
    var dateArrlength = dateArr.length;
    for (var i = 0; i < 6 - finalWeek; i++) {
      let obj = {};
      obj.day = i + 1;
      obj.isLast = true;
      dateArr[dateArrlength + i] = obj;
    }

    this.setData({
      dateArr: dateArr,
      year: year,
      today: today,
      month: month + 1
    })
  },
  // 查看打卡记录
  seeRecord: function(e) {
    var clickpunchindex = e.currentTarget.dataset.clickpunchindex;
    console.log(clickpunchindex);
    var punchClockList = this.data.punchClockList;
    var punchList = [];

    for (var i = 0; i < clickpunchindex.length; i++) {
      var punchObj = punchClockList[clickpunchindex[i]];
      // 判断是否是number类型
      if (typeof(punchObj.punch_clock_date) == "number") {
        punchObj.punch_clock_date = util.formatTime(new Date(punchObj.punch_clock_date * 1000));
      }
      punchList.push(punchObj);
    }
    this.setData({
      punchList: punchList
    });
    this.showRecordToastModal.show();

  },
  /**
   * 查询学生记录
   */
  queryPunchClockList: function(openid) {
    return new Promise((resolve, reject) => {
      let now = new Date();
      let year = now.getFullYear();
      let month = now.getMonth();
      var startTime = util.getTimestamp(new Date(year + "-" + (month + 1) + "-1"));
      var endTime = util.getTimestamp(new Date());
      wx.cloud.callFunction({
        name: "queryStuToPunchClock",
        data: {
          openid: openid,
          startTime: startTime,
          endTime: endTime
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.showRecordToastModal = this.selectComponent("#showRecordToastModal");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})