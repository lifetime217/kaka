// miniprogram/pages/message/index/index.js
var app = getApp();
const db = wx.cloud.database();
var util = require('../../../util/util.js');
Page({

  /**
   * 页面的初始数据
   * 
   * 
   * 
   * 
   */
  data: {
    currentTab: 0,
    messageList: [],
  },
  /**
   * 判断是否是今天的数据 是显示为红色，不是显示的日期
   */
  convertTime: function(data) {
    var date = util.getDate(); // 获取当前的日期
    for (var i = 0; i < data.length; i++) {
      var convertTime = util.formatTime(data[i].addtime * 1000); //转换时间
      if (convertTime.substring(0, 10) == date) {
        data[i].isDay = true; // 数据是今天的
        data[i].addtime = convertTime.substring(11, 16);
      } else {
        data[i].isDay = false; // 数据不是今天的
        data[i].addtime = convertTime.substring(5, 10);
      }
    }
    return data;
  },
  /**
   * 请求消息列表
   * 根据每个用户的openid查询消息列表
   */
  requestMessage: function() {
    var that = this;
    that.showLoad();
    var openid = app.globalData.openid; //获取openid
    //分页查询数据 10条
    wx.cloud.callFunction({
      name: "paginator",
      data: {
        dbName: "message",
        pageIndex: 1,
        pageSize: 10,
        filter: {
          "own_openid": openid,
          "isdelete": 0
        },
        orderBy: ["addtime", "desc"]
      }
    }).then(res => {
      if (res.result != null && res.result.errMsg == "collection.get:ok") {
        var data = res.result.data;
        that.convertTime(data); //处理时间
        that.setData({
          messageList: data
        });
      }
      that.hideTime();
    });
   
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    that.requestMessage();
  },

  // 删除消息
  deleteMessage: function(e) {
    var that = this;
    var id = e.currentTarget.dataset.id;
    wx.showModal({
      title: '提示',
      content: '是否删除该消息',
      success: function(res) {
        if (res.confirm) {
          wx.cloud.callFunction({
            name: "deleteMessage",
            data: {
              id: id
            }
          }).then(res => {
            if (res.result.errMsg == "document.update:ok") {
              that.requestMessage();
            }
          });
        } else if (res.cancel) {}
      }
    })
  }
})