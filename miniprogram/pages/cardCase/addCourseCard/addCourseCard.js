// miniprogram/pages/cardCase/addEnterpriseCard/addEnterpriseCard.js
var util = require('../../../util/util.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: {
      id: 'view',
      name: '年龄范围',
      open: false,
      pages: [{
        value: '1-8'
      }, {
        value: '8-14'
      }, {
        value: '14-18'
      }, {
        value: '18以上',
        checked: 'true'
      }]
    },
    selectIndex: 0,

    classHour: [{
      id: 'classHour',
      name: '课时',
      open: false,
      pages: ['1课时', '2课时']
    }],

    items: [{
        value: '1课时',
        checked: true
      },
      {
        value: '2课时'
      }
    ],



    schooltime: [],
    issubtract: true,

    ageRange: '年龄范围',
    classNa: '1课时',
    it: 1,
    time: [],
    time1: [],
    time2: [],
    formerTime1: '',
    formerTime2: '',
    formerTime3: '',
    formerTime4: '',
    inde: 5,

    scopea: true,
    // scopea_2: false,

    // 是否显示面板指示点
    indicatorDots: false,
    // 是否自动切换
    autoplay: false,
    // 自动切换时间间隔  
    interval: 5000,
    // 滑动动画时长
    duration: 1000,

    // 图片数据
    imgUrls: 'cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png',


    courseName: "", //课程名称
    ageShow: 0, //年龄显示
    introCourse: "", //课程简介

    isdelete: 0, //是否已删除
    course: '', //课程
    courseId: '', //课程id
    enterprise: [], //企业

    triangle: false, //三角倒换
    triangle_2: false,
    status: 'add', //编辑还是新增

  },
  courseInput: function(e) {
    this.setData({
      courseName: e.detail.value
    })
  },
  introduceInput: function(e) {
    this.setData({
      introCourse: e.detail.value
    })
  },




  onAdd: function(e) {
    const db = wx.cloud.database()
    var time1 = this.data.time1;
    var time2 = this.data.time2;
    if (this.data.courseName != "" && this.data.imgUrls != "") {
      if (time1.length == time2.length) {
        var timestamp = Date.parse(new Date());
        var date = new Date(timestamp);

        db.collection('course').add({
          data: {
            courseName: this.data.courseName,
            // ageShow: this.data.ageShow,
            addtime: util.getTimestamp(date),
            courseNum: 0,
            companyId: this.data.enterprise._id,
            companyName: this.data.enterprise.company,
            imagePath: this.data.imgUrls,
            introduce: this.data.introCourse,
            isdelete: this.data.isdelete,
          },
          success: res => {
            wx.showToast({
              title: '新增记录成功',
            })
            console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
            var url = e.currentTarget.dataset.url;
            wx.switchTab({
              url: '../../course/index/index'
            })
          },
          fail: err => {
            wx.showToast({
              icon: 'none',
              title: '新增记录失败'
            })
            console.error('[数据库] [新增记录] 失败：', err)
          }
        })
      } else {
        wx.showToast({
          icon: 'none',
          title: '上课时间需填写完整',
        })
      }

    } else {
      wx.showToast({
        icon: 'none',
        title: '请填写完整',
      })
    }
  },

  addCourse: function(e) {
    const db = wx.cloud.database()
    var time1 = this.data.time1;
    var time2 = this.data.time2;
    if (this.data.courseName != "" && this.data.imgUrls != "") {
      if (time1.length == time2.length) {
        var timestamp = Date.parse(new Date());
        var date = new Date(timestamp);
        db.collection('course').add({
          data: {
            courseName: this.data.courseName,
            // ageShow: this.data.ageShow,
            addtime: util.getTimestamp(date),
            courseNum: 0,
            companyId: this.data.enterprise._id,
            companyName: this.data.enterprise.company,
            imagePath: this.data.imgUrls,
            introduce: this.data.introCourse,
            isdelete: this.data.isdelete,
          },
          success: res => {
            wx.showToast({
              title: '新增记录成功',
            })
            console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
            var url = e.currentTarget.dataset.url;
            wx.navigateTo({
              url: 'addCourseCard'
            })
          },
          fail: err => {
            wx.showToast({
              icon: 'none',
              title: '新增记录失败'
            })
            console.error('[数据库] [新增记录] 失败：', err)
          }
        })
      } else {
        wx.showToast({
          icon: 'none',
          title: '上课时间需填写完整',
        })
      }
    } else {
      wx.showToast({
        icon: 'none',
        title: '请填写完整',
      })
    }
  },


  // 上传图片
  doUpload: function() {
    var that = this;
    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {

        if (res.tempFiles[0].size <= 3000000) {
          wx.showLoading({
            title: '上传中',
          })
          const filePath = res.tempFilePaths[0]
          // 上传图片
          const cloudPath = util.uuid() + filePath.match(/\.[^.]+?$/)[0]
          wx.cloud.uploadFile({
            cloudPath,
            filePath,
            success: res => {
              that.setData({
                imgUrls: filePath,
              });
            },
            fail: e => {
              console.error('[上传文件] 失败：', e)
              wx.showToast({
                icon: 'none',
                title: '上传失败',
              })
            },
            complete: () => {
              wx.hideLoading();
            }
          })
        } else { //图片大于2M，弹出一个提示框
          wx.showToast({
            title: '上传图片不能大于3M!',
            icon: 'none'
          })
          var imgUrls = "cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png";
          that.setData({
            imgUrls: imgUrls,
          });
        }

      },
      fail: e => {
        // console.error(e)
        var imgUrls = "cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png";
        that.setData({
          imgUrls: imgUrls,
        });
      }
    })
  },

  //删除图片
  deleteImage: function(e) {
    var that = this;

    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function(res) {
        if (res.confirm) {
          that.setData({
            imgUrls: "",
          });

        } else if (res.cancel) {
          console.log('点击取消了');
          return false;
        }
      }
    })
  },



  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // var index = 1  //此处可以选择设置 需要选中的下标
    // this.data.items[index].checked = "true"
    // this.setData({
    //   items: this.data.items
    // })

    if (options.status == 'edit') {
      wx.setNavigationBarTitle({
        title: '课程编辑'
      })
      this.setData({
        status: options.status
      })
    }
    // console.log(this.data.status);
    var that = this;
    const db = wx.cloud.database();
    var openid = app.globalData.openid
    if (options.id != null) {
      this.setData({
        courseId: options.id
      });

      db.collection('course').where({
        _id: options.id
      }).get({
        success: res => {
          this.setData({
            course: res.data[0],
            ageShow: res.data[0].ageShow,
            imgUrls: res.data[0].imagePath,
            ageRange: res.data[0].ageRange,
            formerTime1: '09: 00',
            formerTime2: '11: 30',
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '查询记录失败'
          })
        }
      })
    }

    db.collection('enterprise').where({
      _openid: openid
    }).get({
      success: res => {
        this.setData({
          enterprise: res.data[0],
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },



  //  showitem: function() {
  //    this.setData({
  //      open: !this.data.open,
  //    })
  //  },
  //  onshou1: function() {
  //    this.setData({
  //      open: !this.data.open,
  //      ageShow: 1
  //    })
  //  },
  //  onshou2: function() {
  //    this.setData({
  //      open: !this.data.open,
  //      ageShow: 2
  //    })
  //  },
  //  onshou3: function() {
  //    this.setData({
  //      open: !this.data.open,
  //      ageShow: 3
  //    })
  //  },
  //  onshou4: function() {
  //    this.setData({
  //      open: !this.data.open,
  //      ageShow: 4
  //    })
  //  },
  kindRadio: function(e) {
    console.log(e);
    var index = e.currentTarget.dataset.index;
    this.setData({
      selectIndex: index
    });
  },


  //年龄范围
  kindToggle(e) {
    var that = this
    const id = e.currentTarget.id
    const list = that.data.list
    var index = e.currentTarget.dataset.index;
    if (list.id === id) {
      list.open = !list.open
    } else {
      list.open = false
    }


    if (e._relatedInfo.anchorTargetText != '年龄范围') {
      that.setData({
        scopea: false
      })
    }
    if (e._relatedInfo.anchorTargetText != "") {
      that.setData({
        ageRange: e._relatedInfo.anchorTargetText,
      })
    } else {
      that.setData({
        ageRange: e._relatedInfo.anchorRelatedText,
      })
    }


    const triang = that.data.triangle

    that.setData({
      list,
      triangle: !triang,
      selectIndex: index
    })

    // var index = e.currentTarget.dataset.index;
    // this.setData({
    //   selectIndex: index
    // });

  },



  classToggle(e) {
    const id = e.currentTarget.id
    const classHour = this.data.classHour
    for (let i = 0, len = classHour.length; i < len; ++i) {
      if (classHour[i].id === id) {
        classHour[i].open = !classHour[i].open
      } else {
        classHour[i].open = false
      }
    }

    // if (e._relatedInfo.anchorTargetText != '1课时') {
    //   this.setData({
    //     scopea_2: false
    //   })
    // }
    if (e._relatedInfo.anchorTargetText === null) {
      this.setData({
        classNa: e._relatedInfo.anchorRelatedText,
      })
    }


    const triangle_2 = this.data.triangle_2
    this.setData({
      classHour,
      triangle_2: !triangle_2,
      classNa: e._relatedInfo.anchorTargetText,
    })

  },


  //时间段
  addTime: function(e) {
    var that = this;
    var test = this.data.schooltime;

    var time1 = that.data.time1;
    var time2 = that.data.time2;
    var inde = that.data.inde * 1 + 1;

    var row1 = time1.length;
    var row2 = time2.length;
    var testRow = test.length + 1;
    if (row1 != 0 && row2 != 0 && testRow == row1 && testRow == row2) {
      test.push(1);
      var i = 0;
      this.setData({
        schooltime: test,
        inde: inde
      });
    } else {
      wx.showToast({
        icon: 'none',
        title: '请先填写上课时间'
      })
    }

  },
  subtract: function(e) {
    var index = e.currentTarget.dataset.index + 1;
    var time1 = this.data.time1;
    var time2 = this.data.time2;
    time1.splice(index, 1);
    time2.splice(index, 1);

    var test = this.data.schooltime;
    test.splice(e.target.id, 1);
    this.setData({
      schooltime: test,
      time1: time1,
      time2: time2,
    });
  },



  bindTimeChange(e) {
    var that = this;
    // console.log(e);

    var time1 = that.data.time1;
    var time2 = that.data.time2;
    var startTime = e.detail.value;
    var type = e.currentTarget.dataset.type;
    // console.log(type == 0);

    if (type == 0) {
      if (time2[0] != null) {
        if (startTime < time2[0]) {
          time1.splice(0, 1);
          time1.splice(0, 0, startTime);
        } else {
          wx.showToast({
            icon: 'none',
            title: '上课时间要小于下课时间'
          })
        }
      } else {
        time1.splice(0, 1);
        time1.splice(0, 0, startTime);
      }

    } else if (startTime > time1[0]) {
      if (time1[1] != null) {
        for (var i = 1; i < time1.length; i++) {
          if (startTime < time1[i]) {
            time2.splice(0, 1);
            time2.splice(0, 0, startTime);
          } else {
            wx.showToast({
              icon: 'none',
              title: '时间段不能交叉'
            })
          }
        }
      } else {
        time2.splice(0, 1);
        time2.splice(0, 0, startTime);
      }

    } else {
      wx.showToast({
        icon: 'none',
        title: '下课时间要大于上课时间'
      })
    }

    that.setData({
      time1: time1,
      time2: time2,
    })
    console.log(that.data.time);
  },



  bindTimeChange2(e) {
    var that = this;
    // console.log(e);
    var time1 = that.data.time1;
    var time2 = that.data.time2;
    var startTime = e.detail.value;
    var type = e.currentTarget.dataset.type;

    var index = e.currentTarget.dataset.index + 1;

    if (type == 0) {
      if (startTime >= time2[index - 1]) {
        if (time2[index] != null) {
          if (startTime < time2[index]) {
            time1.splice(index, 1);
            time1.splice(index, 0, startTime);
          } else {
            wx.showToast({
              icon: 'none',
              title: '需大于上次下课时间'
            })
          }
        } else {
          time1.splice(index, 1);
          time1.splice(index, 0, startTime);
        }
      } else {
        wx.showToast({
          icon: 'none',
          title: '需大于上次下课时间'
        })
      }


    } else if (startTime > time1[index]) {
      if (time1[index + 1] != null) {
        for (var i = index+1; i < time1.length; i++) {
          if (startTime < time1[i]) {
            time2.splice(index, 1);
            time2.splice(index, 0, startTime);
          } else {
            wx.showToast({
              icon: 'none',
              title: '时间段不能交叉'
            })
          }
        }
      } else {
        time2.splice(index, 1);
        time2.splice(index, 0, startTime);
      }


    } else {
      wx.showToast({
        icon: 'none',
        title: '下课时间需大于上课时间'
      })
    }
    that.setData({
      time1: time1,
      time2: time2,
      inde: index,

    })


  },





})