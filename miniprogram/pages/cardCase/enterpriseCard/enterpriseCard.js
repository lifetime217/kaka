// miniprogram/pages/cardCase/enterpriseCard/enterpriseCard.js
const app = getApp();
var WxParse = require('../../../util/wxParse/wxParse.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 是否显示面板指示点
    indicatorDots: true,
    // 是否自动切换
    autoplay: true,
    // 自动切换时间间隔
    interval: 5000,
    // 滑动动画时长
    duration: 1000,

    primarySize: 'mini',
    id: "", // 名片id
    teaList: [], //企业老师
    enterprise: [], //企业

    isdelete: "", //是否已删除
    roleType: "", //身份 2企业

    html: '', //富文本
    type: "url", // 区别是分享还是跳转
    status: "edit" ,//区分是编辑 新增

    share: '', //分享次数
    examine: '', //查看次数
    clickPhone: '', //电话
    hiddenn: false,//隐藏显示
  },
  // 显示加载框
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  // 隐藏加载框
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
 * 收藏名片夹
 */
  keep: function (e) {
    var phone = e.currentTarget.dataset.phone;
    var name = e.currentTarget.dataset.name;
    wx.addPhoneContact({
      firstName: "A-" + name + " 打卡",
      mobilePhoneNumber: phone,
      success: function (res) {
        console.log(res);
      },
      fail: function (res) {
        console.log(res);
      },
      complete: function (res) {
        console.log(res);
      },
    })
  },
  //地图导航
  cardDetail:function(e){
    var enterprise = this.data.enterprise;

        wx.openLocation({
          latitude: enterprise.latitude,
          longitude: enterprise.longitude,
          scale: 16,
          name: enterprise.roomname,
          address: enterprise.address,
        })
  },
  
  //跳转课程
  toCourse:function(){
    wx.navigateTo({
      // url: '../../course/courseDetail/courseDetail&id=' + id
    })
  },

  /**
   * 生命周期函数--监听页面加载 options跳转传过来的id
   */
  onLoad: function(options) {

    var that = this;
    const db = wx.cloud.database();
    that.showLoad();
   
    // 查询当前用户
    db.collection('enterprise').where({
      _id: options.id
    }).get({
      success: res => {
        // console.log(res);

        this.setData({
          enterprise: res.data[0],
          html: res.data[0].html,
          examine: res.data[0].examine + 1, //查看次数
          share: res.data[0].share,
          clickPhone: res.data[0].phone,
          
           roleType : app.globalData.roleType,
        })
        WxParse.wxParse('article', 'html', res.data[0].html, that, 5);
        that.hideTime();
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })


    //老师
    var openid = app.globalData.openid;
    //调用函数
    that.queryEnterpriseTea(openid).then(function(teaList) {
      console.log(teaList);
        if (teaList.length > 0) {
          that.setData({
            teaList: teaList,
            hiddenn: false,
          });
        } else {
          that.setData({
            isNull: true
          });
        }
      
    })




},
// 查询企业旗下的课程
// queryEnterpriseCour: function(openid) {
//   return new Promise((resolve, reject) => {
//     wx.cloud.callFunction({
//       name: "queryEnterpriseToCour",
//       data: {
//         openid: openid
//       }
//     }).then(res => {
//       resolve(res.courseList);
//     });
//   })
// },

// 查询企业旗下的老师
queryEnterpriseTea: function(openid) {
  return new Promise((resolve, reject) => {
    wx.cloud.callFunction({
      name: "queryEnterpriseToTea",
      data: {
        openid: openid
      }
    }).then(res => {
      resolve(res.result);
    });
  })
},



//电话
callPhone: function(e) {
  wx.makePhoneCall({
    phoneNumber: this.data.clickPhone,
  })
},

edit: function(e) {
  console.log(e.currentTarget.dataset);
  const id = e.currentTarget.id;
  wx.navigateTo({
    url: '../addEnterpriseCard/addEnterpriseCard?status=edit&id=' + id,

  })
},



/**
 * 生命周期函数--监听页面初次渲染完成
 */
onReady: function(options) {
  this.sharemodal = this.selectComponent("#sharemodal");
  // if (this.data.type == "share") {
  //   this.sharemodal.show();
  // }
},
// 取消
cancelModal: function(e) {
  this.sharemodal.hide();
},
// 确定
confirmModal: function(e) {
  // console.log(e.detail.rawData);
  // if (e.detail.rawData != undefined) {
  //   wx.switchTab({
  //     url: '../../init/enterprise/Enterprise',
  //     success: function (res) { },
  //     fail: function (res) { },
  //     complete: function (res) { },
  //   })
  // }
},



/**
 * 生命周期函数--监听页面显示
 */
onShow: function() {

},

/**
 * 生命周期函数--监听页面隐藏
 */
onHide: function() {

},

/**
 * 生命周期函数--监听页面卸载
 */
onUnload: function() {

},

/**
 * 页面相关事件处理函数--监听用户下拉动作
 */
onPullDownRefresh: function() {

},

/**
 * 页面上拉触底事件的处理函数
 */
onReachBottom: function() {

},

/**
 * 用户点击右上角分享
 */
onShareAppMessage: function() {
  var id = this.data.id;
  return {
    title: '邀请老师',
    path: '/pages/course/courseDetail/courseDetail?type=share&id=' + id
  }
  //分享次数
  this.setData({
    examine: this.data.share + 1
  })
}
})