var app = getApp();


Page({
  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    winwidth: 0,
    majorList: [{
      type: '教育辅导',
      List: ['学前辅导', '小学辅导', '中学辅导', '大学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }, {
      type: '幼儿早教',
      List: ['学前教育', '亲子教育', '学前辅导', '小学辅导', '中学辅导']
    }],

    major: ['教育辅导', '幼儿早教', '语言', '艺术/兴趣', '资格认证', '学历教育', '职业技能'],
    OneList: ['学前辅导', '小学辅导', '中学辅导', '大学辅导'],

    key: 0,
    selected: [], //选中
  },
  swichNav: function(e) {
    // console.log(e);
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {

      var current = e.target.dataset.current;
      var OneList = [];
      var majorList = that.data.majorList;
      for (var name in majorList) {
        if (name == current) {
          OneList = majorList[name].List;
          break;
        }
      }
      that.setData({
        OneList: OneList,
        currentTab: e.target.dataset.current
      })
    }
  },
  // //选择
  // btn:function(){
  //   this.setData({
  //     isPitchOn :true
  //   })
  // },

  // 点击添加样式
  clickRegion: function(e) {
    var selected = this.data.selected;
    var newSelected = []; //一个新的临时数组

    selected.push(e.target.dataset.type);
    // if (newSelected.length < 4) {
    //   if (selected.length < 4) {

    //   } else {
    //     wx.showToast({
    //       icon: 'none',
    //       title: '只能选择3个类型'
    //     })
    //   }
    // } else {
    //   wx.showToast({
    //     icon: 'none',
    //     title: '只能选择3个类型'
    //   })
    // }

    for (var i = 0; i < selected.length; i++) {
      var count = 0;
      for (var j = 0; j < selected.length; j++) {
        if (selected[i] == selected[j]) {
          count++;
        }
      }
      if (count == 1) {
        newSelected.push(selected[i]);
      }
    }
   
  
    if (newSelected.length > 3) {
      newSelected.splice(newSelected.length-1,1);
      wx.showToast({
        icon: 'none',
        title: '只能选择3个类型'
      })
    }

    var that = this;
    that.setData({
      key: e.target.dataset.key,
      selected: newSelected
    });

    
  },

//返回类型
  typeReturn:function(){
    var selected = this.data.selected;
    let pages = getCurrentPages(); //当前页面
    let prevPage = pages[pages.length - 2]; //上一页面  
    prevPage.setData({ //直接给上移页面赋值
      data: selected,
    });
    wx.navigateBack({ //返回
      delta: 1
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var OneList = [];




  }
})