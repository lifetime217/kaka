// miniprogram/pages/cardCase/addEnterpriseCard/addEnterpriseCard.js
var util = require('../../../util/util.js');

const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {

    // 图片数据集合
    imgUrls: ['cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png'],


    //是否显示面板指示点
    indicatorDots: false,
    // 是否自动切换
    autoplay: false,
    // 自动切换时间间隔
    interval: 5000,
    // 滑动动画时长
    duration: 1000,
    //滑块下标
    current: 0,
    //地图
    roomname: "",
    latitude: "", //纬度
    longitude: "", //经度

    index: "",
    primarySize: 'mini',

    id: "", // 企业id

    isdelete: 0,
    addtime: "",

    company: "",
    address: "",
    phone: "",
    type: "", //产业类型

    html: '',

    //是否地址显示
    roleType: 0,
    //是否类型显示
    status: 0,

    enterprise: [], //企业
    isshow: 0, //是否显示编辑
  },



  //请求数据
  requestData: function() {
    var that = this;
    wx.showLoading({
      title: '加载中',
      mask: true
    })

    var data = {};
    wx.request({
      url: '',
      data: '',
      header: {
        'content-type': 'application/json'
      },
      method: 'GET',
      dataType: 'json',
      responseType: 'text',
    
    success: function(res) {
      console.log(res);
    }
    })
  },


  bindKeyInput: function(e) {

    this.setData({
      company: e.detail.value
    })
  },
  // siteInput: function(e) {
  //   this.setData({
  //     roomname: e.detail.value
  //   })
  // },
  phoneInput: function(e) {
    this.setData({
      phone: e.detail.value
    })


  },
  typeInput: function(e) {
    this.setData({
      type: e.detail.value
    })
  },

  finish: function(e) {
    // console.log(e.detail.content);
    const db = wx.cloud.database();
    if (this.data.imgUrls.length != 0 && this.data.company != null && this.data.roomname != null && this.data.phone != null && this.data.type != null) {
      if (this.data.phone.length == 11) {
        var timestamp = Date.parse(new Date());
        var date = new Date(timestamp);
        db.collection('enterprise').add({
          data: {
            html: e.detail.content,
            imgUrls: this.data.imgUrls,
            company: this.data.company,
            roomname: this.data.roomname,
            latitude: this.data.latitude, //纬度
            longitude: this.data.longitude, //经度
            phone: this.data.phone,
            type: this.data.type,
            isdelete: this.data.isdelete,
            addtime: util.getTimestamp(date),
          },
          success: res => {

            // var url = e.currentTarget.dataset.url;
            if (this.data.status == 'add') {
              wx.showToast({
                title: '新增企业成功',
              })
              wx.redirectTo({
                url: '../addCourseCard/addCourseCard'
              })
            } else {
              wx.showToast({
                title: '编辑企业成功',
              })
            }
          },
          fail: err => {
            wx.showToast({
              icon: 'none',
              title: '新增企业失败'
            })
            console.error('[数据库] [新增记录] 失败：', err)
          }
        })
      } else {
        wx.showToast({
          icon: 'none',
          title: '请输入11位手机号码',
        })
      }
    } else {
      wx.showToast({
        icon: 'none',
        title: '请填写完整',
      })
    }
  },
  //产业类型
  industryType: function() {
    wx.navigateTo({
      url: '../industryType/industryType'
    })
  },

  // 地图
  cardDetail: function() {
    var that = this;
    if (wx.openSetting) {
      wx.chooseLocation({
        success: function(res) {
          that.setData({
            roomname: res.name,
            address: res.address,
            roleType: 1,
            latitude: res.latitude, //纬度
            longitude: res.longitude, //经度
          })
        }

      })
    }
  },
  //电话
  // callPhone: function(e) {
  //   wx.makePhoneCall({
  //     phoneNumber: e.currentTarget.dataset.phone
  //   })

  // },

  // 上传图片
  doUpload: function() {
    var that = this;
    var imgUrls = [];
    that.setData({
      imgUrls: imgUrls,
    });

    // 选择图片
    wx.chooseImage({
      count: 1,
      sizeType: ['compressed'],
      sourceType: ['album', 'camera'],

      success: function(res) {
        if (that.data.imgUrls.length < 4) {
          if (res.tempFiles[0].size <= 3000000) {
            wx.showLoading({
              title: '上传中',
            })

            const filePath = res.tempFilePaths[0]
            // 上传图片
            const cloudPath = util.uuid() + filePath.match(/\.[^.]+?$/)[0]
            wx.cloud.uploadFile({
              cloudPath,
              filePath,
              success: res => {
                console.log(res);
                console.log(cloudPath);
                console.log(filePath);
                var imgs = that.data.imgUrls;
                imgs.push(filePath);
                that.setData({
                  imgUrls: imgs,
                  current: that.data.imgUrls.length - 1
                });
              },

              fail: e => {
                console.error('[上传文件] 失败：', e)
                wx.showToast({
                  icon: 'none',
                  title: '上传失败',
                })
              },
              complete: () => {
                wx.hideLoading();
              }
            })
          } else { //图片大于2M，弹出一个提示框
            wx.showToast({
              title: '上传图片不能大于3M!',
              icon: 'none'
            })
            var imgUrls = ["cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png"];
            that.setData({
              imgUrls: imgUrls,
            });
          }
        } else {
          wx.showToast({
            icon: 'none',
            title: ' 最多可上传四张照片',
          })
          var imgUrls = ["cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png"];
          that.setData({
            imgUrls: imgUrls,
          });
        }
      },

      fail: e => {
        // console.error(e)
        var imgUrls = ["cloud://dev-2bcb38.6465-dev-2bcb38/01a186fc-f548-4fc3-9e43-da6762f39d81.png"];
        that.setData({
          imgUrls: imgUrls,
        });
      }
    })
  },

  //删除图片
  deleteImage: function(e) {
    var that = this;
    var index = e.currentTarget.dataset.index; //获取当前长按图片下标

    wx.showModal({
      title: '提示',
      content: '确定要删除此图片吗？',
      success: function(res) {
        if (res.confirm) {
          var imgs = that.data.imgUrls;

          imgs.splice(index, 1);

          that.setData({
            imgUrls: imgs,
            current: that.data.imgUrls.length - 1
          });
          console.log(that.data.imgUrls);
        } else if (res.cancel) {
          console.log('点击取消了');
          return false;
        }

      }
    })
  },

  //更新
  renewal: function(e) {
    const db = wx.cloud.database();
    db.collection('enterprise').doc('XAgHyt7E7L4wdJxE').set({
      data: {
        addtime: 123
      },
      success: function(res) {
        console.log(res);
      }
    });
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.status == 'edit') {
      wx.setNavigationBarTitle({
        title: '企业编辑'
      })
      this.setData({
        status: options.status
      })
    }


    var that = this;
    const db = wx.cloud.database()
    const id = options.id;
    if (options.id != null) {
      this.setData({
        id: id
      });

      db.collection('enterprise').where({
        _id: options.id
      }).get({
        success: res => {
          this.setData({
            enterprise: res.data[0],
            isshow: 1,
            imgUrls: res.data[0].imgUrls,
            roomname: res.data[0].roomname,
            type: '小学辅导 ',
            roleType: 1,
          })
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '查询记录失败'
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    let pages = getCurrentPages();
    let currPage = pages[pages.length - 1];
    if (currPage.data.data != null) {

      that.setData({
        type: currPage.data.data,
        status: 1
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },



  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})