// miniprogram/pages/home/punchClock/punchClock.js
var util = require('../../../util/util.js');
var requestData = require('../../../util/request/requestData.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleType: 0,

    teacher: "王老师",

    stuList: [], //学生

    course: [], //课程
    teaList: [], //老师

    clock: [], //打卡信息
    clickList: [],
    // isClock: false,//是否打卡
    clockLis: [], //打卡学生

    stuClok: [],
    clockHas:[], //已经打卡
  },

  //存储打卡学生数组
  clockCase: function(e) {
    var clockLis = this.data.clockLis;


    // console.log(e);
    if (clockLis.length != 0) {

      if (clockLis.indexOf(e.currentTarget.id) > -1) {
        clockLis.splice(clockLis.indexOf(e.currentTarget.id), 1);
      } else {
        clockLis.push(e.currentTarget.id)
        this.setData({
          clockLis: clockLis
        })
      }
    } else {
      clockLis.push(e.currentTarget.id)
      this.setData({
        clockLis: clockLis
      })
    }


  },

  // 打卡
  punchClock: function(e) {

    var that = this;
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp); //打卡时间
    // util.getTimestamp(date)  //时间戳
    // var stuList = that.data.stuList;
    var clockLis = that.data.clockLis;
    var course = that.data.course;
    var teaList = that.data.teaList;

    if (clockLis.length != 0) {
      for (var i = 0; i < clockLis.length; i++) {
        requestData.clockStor(clockLis[i], course, teaList);
      }
    } else {
      wx.showToast({
        icon: 'none',
        title: '请选择学员',
      })
    }
  },
  //刷新打卡人员
  display:function(){
    var clockHas = this.data.clockHas;
    


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;

    const db = wx.cloud.database();
    //查询已经打卡clockHas
    

    // 查询当前用户
    db.collection('course').where({
      _id: options.id,
    }).get({
      success: res => {
        this.setData({
          course: res.data[0],

        })
        // console.log(that.data.course);
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })

    // 查询当前用户
    db.collection('course_stu_relation').where({
      courseId: "XAvV7cDR1TiNuNsO",
    }).get({
      success: res1 => {
        that.setData({
          stuList: res1.data,

        })
        // console.log(that.data.stuList);
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })

    // 查询老师
    db.collection('course_tea_relation').where({
      courseId: "XAvV7cDR1TiNuNsO",
    }).get({
      success: res => {
        that.setData({
          teaList: res.data[0],

        })
        // console.log(that.data.teaList);
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })



  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})