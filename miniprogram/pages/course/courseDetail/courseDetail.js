// miniprogram/pages/home/courseDetail/courseDetail.js
var app = getApp();
var roleType = app.globalData.roleType; //角色的id 
var util = require('../../../util/util.js');
var requestData = require('../../../util/request/requestData.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    roleType: app.globalData.roleType, //角色
    type: "url", // 区别是分享还是跳转


    teacher: "王老师",



    // id: 12,


    imgUrl: "",
    courseName: "",
    introduce: "", //课程介绍
    invitationMessage: '邀请', //邀请信息

    course_id: '',
    isdelete: '',
    openid: app.globalData.openid,

    course: [], //课程集合信息
    // user: [], //用户集合信息
    // teaCourse:[],//课程老师关系

  },
  // 打卡
  punchClock: function(e) {
    // this.sharemodal.show();
    wx.navigateTo({
      url: '../punchClock/punchClock'
    })
  },
  //跳转编辑
  edit: function(e) {
    const id = e.currentTarget.id;
    wx.navigateTo({
      url: '../../cardCase/addCourseCard/addCourseCard?status=edit&id=' + id,

    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options);

    var that = this;
    const db = wx.cloud.database();
    var timestamp = Date.parse(new Date());
    var date = new Date(timestamp); //打卡时间
    var openid = this.data.openid;

    // 查询当前用户的课程
    db.collection('course').where({
      //  companyId: options.company_id,
      _id: options.id,
    }).get({
      success: res => {

        this.setData({
          course: res.data[0],
          course_id: res.data[0]._id,
          isdelete: res.data[0].isdelete,
        })
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '查询记录失败'
        })
      }
    })


    requestData.confirmTeacher(options.id, openid);

    this.sharemodal = this.selectComponent("#sharemodal"); //调用自定义组件
    if (this.data.type == "share") {
      this.sharemodal.show(); //组件显示
    }


    // const companyId = options.companyid;
    const id = options.id;
    this.setData({
      roleType: roleType
    });
    if (options.type != undefined) {
      this.setData({
        type: options.type
      });
    }
  },



  // 取消
  cancelModal: function(e) {
    console.log(2522222233);
    this.sharemodal.hide(); //组件隐藏
  },

  // 确定
  confirmModal: function(e) {
    console.log(111111111111);
    if (e.detail.rawData != undefined) {
      //添加信息
      const db = wx.cloud.database()
      db.collection('course_tea_relation').add({
        data: {
          // course_id: this.data.course_id,
          // // addtime: util.getTimestamp(date),
          // isdelete: this.data.isdelete,
          // tea_openid: e.detail.userInfo.openid,
        },
        success: res => {
          wx.showToast({
            title: '新增记录成功',
          })
          console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id)
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '新增记录失败'
          })
          console.error('[数据库] [新增记录] 失败：', err)
        }
      })



      //跳转页面
      wx.switchTab({
        url: '../../init/enterprise/Enterprise',
        // url: '/pages/course/courseDetail/courseDetail',
        success: function(res) {}, //回调函数
        fail: function(res) {},
        complete: function(res) {},
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function(options) {


  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    console.log("test");
    var id = this.data.course._id;
    var roleType = this.data.roleType;

    if (roleType == 2) {
      // invitationMessage: 邀请你成为此课程老师
      return {
        title: '邀请你成为此课程老师',
        path: '/pages/course/courseDetail/courseDetail?type=share&id=' + id
      }
    }


    if (roleType == 1) {
      // invitationMessage: 邀请你成为此课程学生
      return {
        title: '邀请你成为此课程学生',
        path: '/pages/course/courseDetail/courseDetail?type=share'
      }
    }

  }
})