// miniprogram/pages/home/index/index.js
var app = getApp();
const db = wx.cloud.database();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 课程 普通用户
    courseList: [],
    roleType: 0, //角色类型
    showAll: -1 //点击展示详情
  },
  /**
   * 跳转到公司详情
   */
  navCompany: function(e) {
    const companyid = e.currentTarget.dataset.companyid;
    wx.navigateTo({
      url: '../../cardCase/enterpriseCard/enterpriseCard?companyid=' + companyid
    })

  },
  /**
   * 展示简介所有
   */
  showAll: function(e) {
    var type = e.currentTarget.dataset.type;
    if (type == 0) {
      this.setData({
        showAll: -1
      });
    } else {
      var index = e.currentTarget.dataset.index;
      this.setData({
        showAll: index
      });
    }
  },
  /**
   *   课程详情
   */
  courseDetail: function(e) {
    const id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../courseDetail/courseDetail?id=' + id
    })
  },
  /**
   *  课程统计标签跳转
   */
  swichNav: function(e) {
    this.showLoad();
    this.setData({
      currentTab: e.currentTarget.dataset.currenttab
    });
    this.hideTime();
  },
  /**
   * 显示加载框
   */
  showLoad: function() {
    wx.showLoading({
      title: '加载中...',
      mask: true
    })
  },
  /**
   *  隐藏加载框
   */
  hideTime: function() {
    setTimeout(function() {
      wx.hideLoading();
    }, 1000);
  },
  /**
   * 跳转新增课程按钮
   */
  addCourse: function() {
    wx.navigateTo({
      url: '../../cardCase/addCourseCard/addCourseCard'
    })
  },

  /**
   * 查询课程详细信息
   */
  queryMyCourse: function(roleType, openid, companyId) {
    // 按照顺序执行
    return new Promise((resolve, reject) => {
      wx.cloud.callFunction({
        name: "queryMyCourse",
        data: {
          openid: openid,
          roleType: roleType,
          companyId: companyId
        }
      }).then(res => {
        resolve(res.result);
      });
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    var roleType = app.globalData.roleType;
    that.showLoad();
    var openid = app.globalData.openid;
    var companyId = app.globalData.companyId;
    // if (openid && openid!=""){
    that.queryMyCourse(roleType, openid, companyId).then(function(courseList) {
      that.setData({
        courseList: courseList,
        roleType: roleType
      });
      that.hideTime();
    });
    // }
    // else{
    //   app.openIdCallback = openid => {
    //     if (openid != '') {
    //       that.queryMyCourse(roleType, openid).then(function (courseList) {
    //         console.log(courseList);
    //         that.setData({
    //           courseList: courseList
    //         });
    //         that.hideTime();
    //       });
    //     }
    //   }
    // }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})