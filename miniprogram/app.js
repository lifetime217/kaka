//app.js
import {
  ToastPannel
} from './common/easyModal/toast/toast'

var util = require('util/util.js');

App({
  ToastPannel,
  onLaunch: function() {
    var that = this;
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        traceUser: true,
      })
      // 获取用户的openid和程序的appid
      wx.cloud.callFunction({
        name: "login"
      }).then(res => {
        that.globalData.appId = res.result.event.userInfo.appId;
        // 新增用户
        // wx.cloud.callFunction({
        //   name: "addUser",
        //   data: {
        //     addtime: util.getTimestamp(new Date()),
        //     openid: res.result.event.userInfo.openId
        //   }
        // }).then(res => {
        //   var isNewUser = res.result.isNewUser;
        //   if (!isNewUser) {
        //     var data = res.result.data;
        //     that.globalData.companyId = data[0].companyId;
        //     that.globalData.roleType = data[0].roleType;
        //   } 
        //   that.globalData.openid = res.result.openId;
        //   if (that.openIdCallback) {
        //     console.log(res.result.openId);
        //     that.openIdCallback(res.result.openId);
        //   }
        // });
      });
    }
  },
  globalData: {
    openid: "o1U_m5RQoj_kNNqfGn-rsxC5Xj9Y", //用户的openid
    companyId: "XADW8c6YbCHWbFNe",
    appId: "", //程序的appid
    roleType: 1, //角色类型，0代表普通用户(家长，孩子)   1代表老师   2代表企业     
    searchHistory: [] //搜索历史
  }
})