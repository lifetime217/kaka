// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {

  try {
    // 根据企业id查询企业下的老师
    var teaList = [];
    var companyId = event.companyId;
    // 查询企业下的老师id
    const enterpriseToTeaList = await db.collection("enterprise_user_relation").where({
      "companyId": companyId,
      "roleType": 1,
      "isdelete": 0
    }).field({
      "companyName":true,
      "user_id": true,
      "user_img": true,
      "user_name": true
    }).get();
    var enterpriseToTea = enterpriseToTeaList.data;
    var companyName = enterpriseToTea[0].companyName;
    // 有企业老师时
    if (enterpriseToTea.length > 0) {
      for (var j = 0; j < enterpriseToTea.length; j++) {
        var tea_openid = enterpriseToTea[j].user_id; //老师的openid
        var tea_img = enterpriseToTea[j].user_img;
        var tea_name = enterpriseToTea[j].user_name;
        // 查询老师对应的课程id 多个
        const queryCourseIdList = await db.collection("course_tea_relation").where({
          "tea_openid": tea_openid,
          "isdelete": 0
        }).field({
          "courseId": true,
          "courseName": true
        }).get();
        var courseIdList = queryCourseIdList.data;
        // 老师和课程绑定了的
        if (courseIdList.length > 0) {
          for (var k = 0; k < courseIdList.length; k++) {
            courseIdList[k].companyName = companyName;
            courseIdList[k].companyId = companyId;
            courseIdList[k].user_id = tea_openid;
            courseIdList[k].user_img = tea_img;
            courseIdList[k].user_name = tea_name;
            courseIdList[k].isRelationCourse = true;
            teaList.push(courseIdList[k]);
          }
        }
        // 老师没有和课程绑定
        else {
          enterpriseToTea[j].companyId = companyId;
          enterpriseToTea[j].companyName = companyName;
          enterpriseToTea[j].isRelationCourse = false;
          teaList.push(enterpriseToTea[j]);
        }
      }
    }
    return teaList;
  } catch (e) {
    console.log(e);
  }
}