// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();

// 云函数入口函数
exports.main = async(event, context) => {
  try {
    var openid = event.openid;
    var queryUser = await db.collection("user").where({
      "_openid": openid
    }).get();
    if (queryUser.data.length == 0) {
      return await db.collection('user').add({
        // data 字段表示需新增的 JSON 数据
        data: {
          "_openid": openid,
          "addtime": event.addtime,
          "isEditName": false,
          "old_name": "",
          "new_name": "",
          "user_img": ""
        }
      }).then(res => {
        res.isNewUser = true;
        res.openid= openid;
        return res;
      })
    } else {
      return await db.collection("enterprise_user_relation").where({
        "user_id": openid
      }).field({
        "companyId": true,
        "roleType": true
      }).get().then(res => {
        res.isNewUser = false;
        res.openid = openid;
        return res;
      });
    }

  } catch (e) {
    console.error(e)
  }
}