// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  //根据老师openid查询学生列表
  try {
    const course = await db.collection("course_tea_relation").where({
      "tea_openid": event.tea_openid,
      "isdelete": 0
    }).field({
      "courseId": true
    }).get();
    const courseIdList = course.data;
    var stuList = [];
    for (var i = 0; i < courseIdList.length; i++) {
      var courseId = courseIdList[i].courseId;
      // 查询课程对应的学生
      const queryStuList = await db.collection("course_stu_relation").where({
        "courseId": courseId,
        "isdelete": 0
      }).field({
        "companyId": true,
        "companyName": true,
        "courseId": true,
        "courseName": true,
        "stu_img": true,
        "stu_name": true,
        "stu_openid": true
      }).get();
      var stuObj = queryStuList.data;
      for (var j = 0; j < stuObj.length; j++) {
        stuList.push(stuObj[j]);
      }
    }
    return stuList;
  } catch (e) {
    console.log(e);
  }
}