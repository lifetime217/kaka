// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    return await db.collection('user').where({
      _openid: event.openid
    }).update({
      // data 字段表示需新增的 JSON 数据
      data: {
        roleType: event.roleType //修改角色
      }
    })
  } catch (e) {
    console.error(e)
  }
}