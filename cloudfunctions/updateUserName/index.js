// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    if (!event.isEditName) {
      return await db.collection('user').where({
        _openid: event.openid
      }).update({
        data: {
          isEditName: false,
          old_name: event.old_name,
          user_img: event.user_img
        }
      });
    } else {
      return await db.collection('user').where({
        _openid: event.openid
      }).update({
        data: {
          isEditName: true,
          new_name: event.new_name
        }
      });
    }
  } catch (e) {
    console.error(e)
  }
}