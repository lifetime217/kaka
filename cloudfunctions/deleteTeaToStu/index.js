// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
  try {
    var id = event.id;
    return await db.collection('course_stu_relation').doc(id).update({
      data: {
        isdelete: 1
      }
    })
  } catch (e) {
    console.log(e);
  }
}