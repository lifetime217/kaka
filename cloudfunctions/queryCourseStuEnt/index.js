// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    var roleType = event.roleType;
    var companyId = event.companyId;
    var tea_openid = event.openid;
    var courseList = [];

    var openid = event.openid;
    var courseList = [];
    if (roleType == 2) {
      const queryCourse = await db.collection("course").where({
        "companyId": companyId,
        "isdelete": 0
      }).field({
        "_id": true,
        "ageShow": true,
        "companyName": true,
        "courseName": true,
        "courseName": true,
        "courseNum": true,
        "imagePath": true
      }).get();

      var courseObj = queryCourse.data;
      console.log(courseObj);
      if (courseObj.length > 0) {
        for (var j = 0; j < courseObj.length; j++) {
          var course = courseObj[j];
          console.log(course);
          const teaList = await db.collection("course_tea_relation").where({
            "courseId": course._id,
            "isdelete": 0
          }).field({
            "tea_name": true
          }).get();
          var teaobj = teaList.data;
          console.log(teaobj);
          var teaName = "";
          if (teaobj.length > 0) {
            for (var k = 0; k < teaobj.length; k++) {
              if (teaName == "") {
                teaName = teaobj[k].tea_name;
              } else {
                teaName = teaobj[k].tea_name + "," + teaName;
              }
            }
            course.teaName = teaName;
            course.isHaveTea = true;
            courseList.push(course);
          } else {
            course.isHaveTea = false;
            courseList.push(course);
          }
        }
      } 
    } else if (roleType == 1) {

      const queryCourse = await db.collection("course_tea_relation").where({
        "tea_openid": tea_openid,
        "isdelete": 0
      }).field({
        "_id": true,
        "ageShow": true,
        "companyId": true,
        "companyName": true,
        "courseName": true,
        "courseId": true,
        "tea_name": true
      }).get();

      var courseObj = queryCourse.data;
      console.log(courseObj);
      if (courseObj.length > 0) {
        for (var j = 0; j < courseObj.length; j++) {
          var course = courseObj[j];
          console.log(course);
          const teaList = await db.collection("course_tea_relation").where({
            "courseId": course._id,
            "isdelete": 0
          }).field({
            "tea_name": true
          }).get();
          var teaobj = teaList.data;
          console.log(teaobj);
          var teaName = "";
          if (teaobj.length > 0) {
            for (var k = 0; k < teaobj.length; k++) {
              if (teaName == "") {
                teaName = teaobj[k].tea_name;
              } else {
                teaName = teaobj[k].tea_name + "," + teaName;
              }
            }
            course.teaName = teaName;
            course.isHaveTea = true;
            courseList.push(course);
          } else {
            course.isHaveTea = false;
            courseList.push(course);
          }
        }
      } 

    }

    return courseList;
  } catch (e) {
    console.log(e);
  }
}