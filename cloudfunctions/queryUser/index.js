// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    // 查询个人信息
    return await db.collection("user").where({
      "_openid": event.openid
    }).field({
      "isEditName": true,
      "new_name": true,
      "old_name": true,
      "user_img": true
    }).get();
  } catch (e) {
    console.log(e);
  }
}