// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    // 根据查询学生打卡记录
    var openid = event.openid;
    var startTime = event.startTime;
    var endTime = event.endTime;
    var _ = db.command;
    var punchClockList = await db.collection("punch_clock_record").where({
      "stu_openid": openid,
      "isdelete": 0,
      "punch_clock_date": _.gte(startTime).and(_.lte(endTime))
    }).field({
      "companyId": true,
      "companyName": true,
      "courseId": true,
      "courseName": true,
      "punch_clock_date": true,
      "tea_name": true,
      "tea_openid": true
    }).get();
    var res ={};
    res.punchClockList = punchClockList.data;

    var punchCount = await db.collection("punch_clock_record").where({
      "stu_openid": openid,
      "isdelete": 0
    }).count();
    res.punchCount = punchCount.total;
    return  res;

  } catch (e) {
    console.log(e);
  }
}