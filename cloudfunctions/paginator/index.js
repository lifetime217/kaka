// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database();

// 云函数入口函数   分页
exports.main = async(event, context) => {
  var dbName = event.dbName; //集合名称
  var filter = event.filter ? event.filter : null; //筛选条件,默认为空  格式{_id:'xxxxxx'}
  var orderByOne = event.orderBy ? event.orderBy : null; //判断是否有排序
  var pageIndex = event.pageIndex ? event.pageIndex : 1; //当前第几页，默认为第一页
  var pageSize = event.pageSize ? event.pageSize : 10; //每页取多少记录，默认为10条
  const countResult = await db.collection(dbName).where(filter).count(); //获取集合中的总记录数
  const total = countResult.total; //获取总记录数
  //计算需要多少页
  const totalPage = Math.ceil(total / pageSize);
  var hasMore; //提示全段是否还有数据
  if (pageIndex > totalPage || pageIndex == totalPage) { //如果没有数据，就返回false
    hasMore = false;
  } else {
    hasMore = true;
  }
  if (orderByOne == null) {
    return db.collection(dbName).where(filter).skip((pageIndex - 1) * pageSize).limit(pageSize).get().then(res => {
      res.hasMore = hasMore;
      return res;
    })
  } else {
    return db.collection(dbName).where(filter).skip((pageIndex - 1) * pageSize).limit(pageSize).orderBy(orderByOne[0], orderByOne[1]).get().then(res => {
      res.hasMore = hasMore;
      console.log(res);
      return res;
    })
  }

}