// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();
// 云函数入口函数
exports.main = async(event, context) => {
  try {
    var stuList = [];
    var companyId = event.companyId;
    // 查询企业下的学生列表  
    const enterpriseToStuList = await db.collection("enterprise_user_relation").where({
      "companyId": companyId,
      "roleType": 0,
      "isdelete": 0
    }).field({
      "companyName":true,
      "user_id": true,
      "user_img": true,
      "user_name": true
    }).get();
    var enterpriseToStu = enterpriseToStuList.data;
    var companyName = enterpriseToStu[0].companyName; //公司名
    // 企业下有学生
    if (enterpriseToStu.length > 0) {
      for (var j = 0; j < enterpriseToStu.length; j++) {
        var stu_openid = enterpriseToStu[j].user_id; //学生openid
        var stu_img = enterpriseToStu[j].user_img;
        var stu_name = enterpriseToStu[j].user_name;
        // 根据学生的id查询课程的id
        const queryCourseIdList = await db.collection("course_stu_relation").where({
          "stu_openid": stu_openid,
          "isdelete": 0
        }).field({
          "courseId": true,
          "courseName": true
        }).get();
        var courseIdList = queryCourseIdList.data;
        if (courseIdList.length > 0) {
          for (var k = 0; k < courseIdList.length; k++) {
            courseIdList[k].user_id = stu_openid;
            courseIdList[k].user_img = stu_img;
            courseIdList[k].user_name = stu_name;
            courseIdList[k].companyId = companyId;
            courseIdList[k].companyName = companyName;
            courseIdList[k].isRelationCourse = true;
            stuList.push(courseIdList[k]);
          }
        }
        // 学生没有和课程关联的时候
        else {
          enterpriseToStu[j].companyId = companyId;
          enterpriseToStu[j].companyName = companyName;
          enterpriseToStu[j].isRelationCourse = false;
          stuList.push(enterpriseToStu[j]);
        }
      }
    }

    return stuList;
  } catch (e) {
    console.log(e);
  }
}